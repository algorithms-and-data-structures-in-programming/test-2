class board:
    def board_size(self, w, h, n):
        min_size = 1
        max_size = max(w, h) * n

        while min_size < max_size:
            mid_point = (min_size + max_size) // 2

            if (mid_point // w) * (mid_point // h) >= n:
                max_size = mid_point
            else:
                min_size = mid_point + 1

        return min_size

#test :)
board = board()
w = int(input())
h = int(input())
n = int(input())
minimum_board_size = board.board_size(w, h, n)
print(minimum_board_size)
