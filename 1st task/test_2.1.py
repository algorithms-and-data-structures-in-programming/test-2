class Stones:
    def weight_diff(self, num_stones, weights):
        min_diff = 99999999

        for i in range(1, 2 ** num_stones):
            weight_1 = 0
            for j in range(num_stones):
                if i & (1 << j):
                    weight_1 += weights[j]

            weight_2 = sum(weights) - weight_1
            diff = abs(weight_1 - weight_2)

            if diff < min_diff:
                min_diff = diff

        return min_diff


stones = Stones()
num_stones = 5
weights = [1, 2, 3, 4, 5]
print(stones.weight_diff(num_stones, weights))

#2 + 3 + 4 = 7  1 + 5 = 6 раз 1

